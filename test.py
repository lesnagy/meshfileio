from math import *
from meshfileio import *

def calculateField(vertices, field):
    for i in range(len(vertices)):
        v = vertices[i]
        f = field[i]

        xsigma = 0.5
        ysigma = 0.5

        f.x = -v.y
        f.y =  v.x
        f.z =  1.0 * exp(-1.0*( (f.x-0.0)**2/xsigma +  (f.y-0.0)**2/ysigma ) )

        norm = sqrt(f.x**2 + f.y**2 + f.z**2)

        f.x = f.x/norm
        f.y = f.y/norm
        f.z = f.z/norm

vertices  = VertexList3D()
elements  = TupleList4()

loadFromPatran3D('highres.pat', vertices, elements, ONE_INDEXING)

field     = VectorField3D(len(vertices))
variables = VariableList(['X', 'Y', 'Z', 'Mx', 'My', 'Mz'])
calculateField(vertices, field)

#for v in vertices:
#    print("{0:20.15f} {1:20.15f} {2:20.15f}".format(v.x, v.y, v.z))

#for e in elements:
#    print("[{0:5d}] {1:5d} {2:5d} {3:5d} {4:5d}".format(e.sid, e.n0, e.n1, e.n2, e.n3))

saveToTecplot('out.tec', vertices, field, elements, variables, 1, ONE_INDEXING) 
