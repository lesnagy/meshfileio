#include "TecplotWriter.h"

#include <algorithm>
#include <iomanip>

TecplotWriter::TecplotWriter() : Writer("")
{}

TecplotWriter::TecplotWriter(const std::string & fileName) : Writer(fileName)
{}

TecplotWriter::~TecplotWriter()
{}

void TecplotWriter::writeTitle(const std::string & title)
{
  if (mFout.is_open()) {
    // Make a local copy of title
    std::string lclTitle(title);

    // Remove all occurences of '"'
    lclTitle.erase(std::remove(lclTitle.begin(), lclTitle.end(), '"'), lclTitle.end());

    // Write output
    mFout << "TITLE = \"" << lclTitle << "\"" << std::endl;
  }
}

void TecplotWriter::writeVariables(const std::vector< std::string > & names)
{
  if (mFout.is_open()) {
    if (names.size() > 0) {
      mFout << "VARIABLES = ";
      for (size_t i = 0; i < names.size(); ++i) {
        // Retrieve the string
        std::string s(names[i]);

        // Remove all occurences of '"'
        s.erase(std::remove(s.begin(), s.end(), '"'), s.end());

        // Write output
        mFout << "\"" << s << "\"";

        // Write a comma if needed
        if (i != names.size()-1) {
          mFout << ",";
        }
      }
      mFout << std::endl;
    }
  }
}

void TecplotWriter::writeZone(int                 t, 
                              int                 n, 
                              int                 e, 
                              const std::string & fePoint, 
                              const std::string & elementType)
{
  if (mFout.is_open()) {
    mFout << "ZONE T=\"" << std::right << std::setw(9) << t << "\"" 
      << " N=" << std::left << std::setw(9) << n << ","
      << " E=" << std::left << std::setw(9) << e << "\n"
      << " F=" << fePoint << ", " << "ET=" << elementType << std::endl;
  }
}

void TecplotWriter::writeZone(int                 t, 
                              int                 n, 
                              int                 e, 
                              const std::string & fePoint, 
                              const std::string & elementType,
                              int                 varshareListStart, 
                              int                 varshareListEnd, 
                              int                 varshareListSelected,
                              int                 connectivityShareZone)
{
  if (mFout.is_open()) {
    mFout << " ZONE T=\"" << std::right << std::setw(9) << t << "\"" 
      << " N=" << std::left << std::setw(9) << n << ","
      << " E=" << std::left << std::setw(9) << e << "\n"
      << " F=" << fePoint << ", " << "ET=" << elementType << ","
      << " VARSHARELIST=([" << varshareListStart << "-" 
                            << varshareListEnd << "]=" 
                            << varshareListSelected << "),"
      << " CONNECTIVITYSHAREZONE=" << connectivityShareZone << std::endl;
  }
}

void TecplotWriter::writeVertexField(double  x, double  y, double  z,
                                     double fx, double fy, double fz)
{
  if (mFout.is_open()) {
    mFout 
      << std::right << std::fixed << std::setw(10) << std::setprecision(6) << x
      << std::right << std::fixed << std::setw(10) << std::setprecision(6) << y
      << std::right << std::fixed << std::setw(10) << std::setprecision(6) << z
      << std::right << std::fixed << std::setw(10) << std::setprecision(6) << fx
      << std::right << std::fixed << std::setw(10) << std::setprecision(6) << fy
      << std::right << std::fixed << std::setw(10) << std::setprecision(6) << fz
      << std::endl;
  }
}

void TecplotWriter::writeField(double fx, double fy, double fz)
{
  if (mFout.is_open()) {
    mFout 
      << std::right << std::fixed << std::setw(10) << std::setprecision(6) << fx << "  "
      << std::right << std::fixed << std::setw(10) << std::setprecision(6) << fy << "  "
      << std::right << std::fixed << std::setw(10) << std::setprecision(6) << fz
      << std::endl;
  }
}

void TecplotWriter::writeElement(int n0, int n1, int n2, int n3)
{
  if (mFout.is_open()) {
    mFout
      << std::right << std::setw(9) << n0
      << std::right << std::setw(9) << n1
      << std::right << std::setw(9) << n2
      << std::right << std::setw(9) << n3
      << std::endl;
  }
}

void TecplotWriter::writeElement(int n0, int n1, int n2)
{
  if (mFout.is_open()) {
    mFout
      << std::right << std::setw(9) << n0
      << std::right << std::setw(9) << n1
      << std::right << std::setw(9) << n2
      << std::endl;
  }
}

