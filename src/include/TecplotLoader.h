/**
 * \file   TecplotLoader.h
 * \author L. Nagy
 *
 * Copyright [2016] Lesleis Nagy. All rights reserved.
 */

#ifndef SRC_IO_TECPLOTLOADER_H_
#define SRC_IO_TECPLOTLOADER_H_

#include <string>

#include "TecplotObserver.h"

/**
 * \brief Class to load a Tecplot file.
 *
 * The TecplotLoader class contais functionality to read Tecplot files and
 * extract basic data/metadata in to primitive data structures. The Tecplot
 * file may contain multiple zones (i.e. several fields corresponding with
 * vertex positions).
 */
class TecplotLoader
{
public:
  TecplotLoader(TecplotObserver & observer);
  void load(const std::string & filename);
private:
  TecplotObserver & mObserver;
};

#endif  // SRC_IO_TECPLOTLOADER_H_
