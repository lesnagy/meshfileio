#ifndef PATRAN_WRITER_H_
#define PATRAN_WRITER_H_

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "Writer.h"

class PatranWriter : public Writer
{
public:
  PatranWriter();

  PatranWriter(const std::string & fileName);

  ~PatranWriter();

  void writeTitle(const std::string & title);
  
  void writeHeader(int nvert, int nelem);

  void writeVertex(int index, double x, double y, double z);
 
  void writeElement(int id, int sid, int n0, int n1, int n2, int n3);

  void writeElement(int id, int sid, int n0, int n1, int n2);

  void writeMaterialProperties(const std::vector<double> & values);

  void writeElementProperties(int sid, double value);

  void writeEnd();

};

#endif  // PATRAN_WRITER_H_
