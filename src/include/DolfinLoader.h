#ifndef DOLFIN_LOADER_H_
#define DOLFIN_LOADER_H_

#include <string>
#include <vector>

#include "DolfinObserver.h"

class DolfinLoader
{
public:
  DolfinLoader(DolfinObserver & observer);
  void load(const std::string & meshFileName,
            const std::string & meshFunctionFileName);
private:
  DolfinObserver   & mObserver;
  std::vector<int>   mSubmeshIds;

  void initSubmeshIds(const std::string & meshFunctionFileName);
};

#endif  // DOLFIN_LOADER_H_
