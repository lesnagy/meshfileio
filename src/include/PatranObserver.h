#ifndef PATRAN_OBSERVER_H_
#define PATRAN_OBSERVER_H_

class PatranObserver
{
public:
  inline virtual ~PatranObserver() {}
  virtual bool vertex      (double x, double y, double z)                       = 0;
  virtual bool element     (int eidx, int sidx, int n0, int n1, int n2, int n3) = 0;
  virtual bool elementType (int tidx)                                           = 0;
};

typedef PatranObserver* PatranObserverPtr;

#endif  // PATRAN_OBSERVER_H_

