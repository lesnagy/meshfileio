#ifndef TECPLOT_WRITER_H_
#define TECPLOT_WRITER_H_

#include <string>
#include <vector>

#include "Writer.h"

class TecplotWriter : public Writer
{
public:
  TecplotWriter();

  TecplotWriter(const std::string & fileName);

  ~TecplotWriter();

  void writeTitle(const std::string & title);

  void writeVariables(const std::vector< std::string > & names);

  void writeZone(int                 t, 
                 int                 n, 
                 int                 e, 
                 const std::string & fePoint, 
                 const std::string & elementType);

  void writeZone(int                 t, 
                 int                 n, 
                 int                 e, 
                 const std::string & fePoint, 
                 const std::string & elementType,
                 int                 varshareListStart, 
                 int                 varshareListEnd, 
                 int                 varshareListSelected,
                 int                 connectivityShareZone);


  void writeVertexField(double  x, double  y, double z, 
                        double fx, double fy, double fz);

  void writeField(double fx, double fy, double fz);

  void writeElement(int n0, int n1, int n2, int n3);

  void writeElement(int n0, int n1, int n2);
};

#endif  // TECPLOT_WRITER_H_
