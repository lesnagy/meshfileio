#ifndef TECPLOT_LOADER_IMPL_H_
#define TECPLOT_LOADER_IMPL_H_

#include <string>
#include <vector>

#include "TecplotObserver.h"

#include "Field2D.hpp"
#include "Field3D.hpp"
#include "Vertex2D.hpp"
#include "Vertex3D.hpp"
#include "Tuple3.hpp"
#include "Tuple4.hpp"

#include "ElementIndexing.h"

class TecplotObserverImpl : public TecplotObserver
{
public:
  TecplotObserverImpl(std::vector<Vertex3D>    & vert,
                      std::vector<Field3D>     & field,
                      std::vector<Tuple4>      & elements,
                      std::vector<std::string> & variables);
 
  ~TecplotObserverImpl();

  virtual void title (std::string title);

  virtual void variable (std::string name);

  virtual void pointType (std::string typeName);

  virtual void elementType (std::string typeName);

  virtual void vertex (double  x, double  y, double  z);

  virtual void field (double fx, double fy, double fz);

  virtual void element (int n0, int n1, int n2, int n3);

  virtual void tvalue (int t);

  virtual void nverts (int n);

  virtual void nelems (int e);

  virtual void zone ();

  int tvalue () const;

  int nzones () const;

  std::string title () const;

  std::string pointType () const;

  std::string elementType () const;

  int nvert () const;

  int nelem () const;

private:
  std::vector<Vertex3D>    & mVert;
  std::vector<Field3D>     & mField;
  std::vector<Tuple4>      & mElements;
  std::vector<std::string> & mVariables;

  int         mTValue;
  int         mNZones;
  std::string mTitle;
  std::string mPointType;
  std::string mElementType;
  int         mNVert;
  int         mNElem;
};

void loadFromTecplot(const std::string        & fileName,
                     std::vector<Vertex3D>    & vert,
                     std::vector<Field3D>     & field,
                     std::vector<Tuple4>      & elements,
                     std::vector<std::string> & variables,
                     int                      * nzones);

void saveToTecplot(const std::string          & fileName,
                     std::vector<Vertex3D>    & vert,
                     std::vector<Field3D>     & field,
                     std::vector<Tuple4>      & elements,
                     std::vector<std::string> & variables,
                     int                        nzones);
                   

#endif  // TECPLOT_LOADER_IMPL_H_
