#ifndef DOLFIN_IO_IMPL_H_
#define DOLFIN_IO_IMPL_H_

#include <string>
#include <vector>

#include "DolfinLoader.h"
#include "DolfinObserver.h"

#include "Field2D.hpp"
#include "Field3D.hpp"
#include "Vertex2D.hpp"
#include "Vertex3D.hpp"
#include "Tuple3.hpp"
#include "Tuple4.hpp"

#include "ElementIndexing.h"

///////////////////////////////////////////////////////////////////////////////
// 3D Stuff                                                                  //
///////////////////////////////////////////////////////////////////////////////

class DolfinObserverImpl3D : public DolfinObserver
{
public:
  DolfinObserverImpl3D(std::vector<Vertex3D> & vertices,
                       std::vector<Tuple4>   & elements,
                       ElementIndexing         indexing);

  ~DolfinObserverImpl3D();

  virtual void vertex(double x, double y, double z);

  virtual void element(int idx, int sid, int n0, int n1, int n2, int n3);

private:
  ElementIndexing         mIndexing;
  std::vector<Vertex3D> & mVertices;
  std::vector<Tuple4>   & mElements;
};

void loadFromDolfin3D(const std::string     & fileName,
                      const std::string     & meshFunFileName,
                      std::vector<Vertex3D> & vertices,
                      std::vector<Tuple4>   & elements,
                      ElementIndexing         indexing);


void saveToDolfin3D(const std::string       & fileName,
                    const std::string     & meshFunFileName,
                    std::vector<Vertex3D>   & vertices,
                    std::vector<Tuple4>     & elements,
                    ElementIndexing           indexing);

#endif  // DOLFIN_IO_IMPL_H_
