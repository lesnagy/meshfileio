#ifndef FIELD2D_HPP_
#define FIELD2D_HPP_

#include <cmath>
#include <string>
#include <sstream>
#include <iomanip>

class Field2D
{
public:
  Field2D():
    x(0.0), y(0.0)
  {}

  Field2D(double new_x, double new_y):
    x(new_x), y(new_y)
  {}

  Field2D operator+ (const Field2D & rhs) const
  {
    return Field2D(x+rhs.getx(), y+rhs.gety());
  }

  Field2D operator- (const Field2D & rhs) const
  {
    return Field2D(x-rhs.getx(), y-rhs.gety());
  }

  Field2D operator/ (double scalar) const
  {
    return Field2D(x/scalar, y/scalar);
  }

  Field2D operator* (double scalar) const
  {
    return Field2D(x*scalar, y*scalar);
  }

  double norm() const
  {
    return sqrt(x*x + y*y);
  }

  std::string __str__() const {
    std::stringstream ss;

    ss << "(";
    ss << std::fixed << std::setw(11) << std::setprecision(6) << x << ", ";
    ss << std::fixed << std::setw(11) << std::setprecision(6) << y;
    ss << ")";

    return ss.str();
  }

  inline double getx() const { return x; }
  inline double gety() const { return y; }

  double x;
  double y;
};

#endif  // FIELD2D_HPP_
