#ifndef PATRAN_LOADER_IMPL_H_
#define PATRAN_LOADER_IMPL_H_

#include <string>
#include <vector>

#include "PatranObserver.h"

#include "Field2D.hpp"
#include "Field3D.hpp"
#include "Vertex2D.hpp"
#include "Vertex3D.hpp"
#include "Tuple3.hpp"
#include "Tuple4.hpp"

#include "ElementIndexing.h"
#include "ElementType.h"

////////////////////////////////////////////////////////////////////////////////
// An observer that can be used to get the element type                       //
////////////////////////////////////////////////////////////////////////////////

class PatranObserverImplType : public PatranObserver
{
public:

  PatranObserverImplType();

  ~PatranObserverImplType();

  virtual bool vertex(double x, double y, double z);

  virtual bool element(int idx, int sid, int n0, int n1, int n2, int n3);

  virtual bool elementType(int tid);

  ElementType getElementType();

private:
  ElementType mElementType;
};

ElementType getPatranMeshType(const std::string & fileName);

///////////////////////////////////////////////////////////////////////////////
// 2D Stuff                                                                  //
///////////////////////////////////////////////////////////////////////////////

class PatranObserverImpl2D : public PatranObserver
{
public:
  PatranObserverImpl2D(std::vector<Vertex2D> & vertices,
                       std::vector<Tuple3>   & elements,
                       ElementIndexing         indexing);

  ~PatranObserverImpl2D();

  virtual bool vertex(double x, double y, double z);

  virtual bool element(int idx, int sid, int n0, int n1, int n2, int n3);

  virtual bool elementType(int tid);

private:
  ElementIndexing         mIndexing;
  ElementType             mElementType;
  std::vector<Vertex2D> & mVertices;
  std::vector<Tuple3>   & mElements;

};

void loadFromPatran2D(const std::string     & fileName,
                      std::vector<Vertex2D> & vertices,
                      std::vector<Tuple3>   & elements,
                      ElementIndexing         indexing);

void saveToPatran2D(const std::string       & fileName,
                    std::vector<Vertex2D>   & vertices,
                    std::vector<Tuple3>     & elements,
                    ElementIndexing           indexing);

///////////////////////////////////////////////////////////////////////////////
// 3D Stuff                                                                  //
///////////////////////////////////////////////////////////////////////////////

class PatranObserverImpl3D : public PatranObserver
{
public:
  PatranObserverImpl3D(std::vector<Vertex3D> & vertices,
                       std::vector<Tuple4>   & elements,
                       ElementIndexing         indexing);

  ~PatranObserverImpl3D();

  virtual bool vertex(double x, double y, double z);

  virtual bool element(int idx, int sid, int n0, int n1, int n2, int n3);

  virtual bool elementType(int tid);

private:
  ElementIndexing         mIndexing;
  ElementType             mElementType;
  std::vector<Vertex3D> & mVertices;
  std::vector<Tuple4>   & mElements;
};

void loadFromPatran3D(const std::string     & fileName,
                      std::vector<Vertex3D> & vertices,
                      std::vector<Tuple4>   & elements,
                      ElementIndexing         indexing);


void saveToPatran3D(const std::string       & fileName,
                    std::vector<Vertex3D>   & vertices,
                    std::vector<Tuple4>     & elements,
                    ElementIndexing           indexing);

#endif  // PATRAN_LOADER_IMPL_H_
