#ifndef VERTEX3D_HPP_
#define VERTEX3D_HPP_

class Vertex3D
{
public:
  Vertex3D():
    x(0.0), y(0.0), z(0.0)
  {}

  Vertex3D(double new_x, double new_y, double new_z):
    x(new_x), y(new_y), z(new_z)
  {}
  
  Vertex3D operator+ (const Vertex3D & rhs) const
  {
    return Vertex3D(x+rhs.getx(), y+rhs.gety(), z+rhs.getz());
  }

  Vertex3D operator- (const Vertex3D & rhs) const
  {
    return Vertex3D(x-rhs.getx(), y-rhs.gety(), z-rhs.getz());
  }

  Vertex3D operator/ (double scalar) const
  {
    return Vertex3D(x/scalar, y/scalar, z/scalar);
  }

  Vertex3D operator* (double scalar) const
  {
    return Vertex3D(x*scalar, y*scalar, z*scalar);
  }

  double norm() const
  {
    return sqrt(x*x + y*y + z*z);
  }

  double dot(const Vertex3D & rhs) const 
  {
    return x*rhs.getx() + y*rhs.gety() + z*rhs.getz();
  }

  Vertex3D cross(const Vertex3D & rhs) const 
  {
    return Vertex3D(
        -rhs.gety()*z + rhs.getz()*y,
         rhs.getx()*z - rhs.getz()*x,
        -rhs.getx()*y + rhs.gety()*x);
  }

  std::string __str__() const {
    std::stringstream ss;

    ss << "(";
    ss << std::fixed << std::setw(11) << std::setprecision(6) << x << ", ";
    ss << std::fixed << std::setw(11) << std::setprecision(6) << y << ", ";
    ss << std::fixed << std::setw(11) << std::setprecision(6) << z;
    ss << ")";

    return ss.str();
  }

  inline double getx() const { return x; }
  inline double gety() const { return y; }
  inline double getz() const { return z; }

  double x;
  double y;
  double z;
};

#endif  // VERTEX3D_HPP_
