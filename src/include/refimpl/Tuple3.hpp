#ifndef TUPLE3_HPP_
#define TUPLE3_HPP_

class Tuple3
{
public:
  Tuple3():
    n0(0), n1(0), n2(0), sid(0)
  {}

  Tuple3(int new_sid, int new_n0, int new_n1, int new_n2):
    sid(new_sid), n0(new_n0), n1(new_n1), n2(new_n2)
  {}

  Tuple3(int new_n0, int new_n1, int new_n2):
    sid(0), n0(new_n0), n1(new_n1), n2(new_n2)
  {}

  int sid;
  int n0;
  int n1;
  int n2;
};

#endif  // TUPLE3_HPP_
