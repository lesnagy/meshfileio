#ifndef VERTEX2D_HPP_
#define VERTEX2D_HPP_

class Vertex2D
{
public:
  Vertex2D():
    x(0.0), y(0.0)
  {}

  Vertex2D(double new_x, double new_y):
    x(new_x), y(new_y)
  {}

  Vertex2D operator+ (const Vertex2D & rhs) const
  {
    return Vertex2D(x+rhs.getx(), y+rhs.gety());
  }

  Vertex2D operator- (const Vertex2D & rhs) const
  {
    return Vertex2D(x-rhs.getx(), y-rhs.gety());
  }

  Vertex2D operator/ (double scalar) const
  {
    return Vertex2D(x/scalar, y/scalar);
  }

  Vertex2D operator* (double scalar) const
  {
    return Vertex2D(x*scalar, y*scalar);
  }

  double norm() const
  {
    return sqrt(x*x + y*y);
  }

  std::string __str__() const {
    std::stringstream ss;

    ss << "(";
    ss << std::fixed << std::setw(11) << std::setprecision(6) << x << ", ";
    ss << std::fixed << std::setw(11) << std::setprecision(6) << y;
    ss << ")";

    return ss.str();
  }

  inline double getx() const { return x; }
  inline double gety() const { return y; }

  double x;
  double y;
};

#endif  // VERTEX2D_HPP_
