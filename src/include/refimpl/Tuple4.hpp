#ifndef TUPLE4_HPP_
#define TUPLE4_HPP_

class Tuple4
{
public:
  Tuple4():
    n0(0), n1(0), n2(0), n3(0), sid(0)
  {}

  Tuple4(int new_sid, int new_n0, int new_n1, int new_n2, int new_n3):
    sid(new_sid), n0(new_n0), n1(new_n1), n2(new_n2), n3(new_n3)
  {}

  Tuple4(int new_n0, int new_n1, int new_n2, int new_n3):
    sid(0), n0(new_n0), n1(new_n1), n2(new_n2), n3(new_n3)
  {}

  int sid;
  int n0;
  int n1;
  int n2;
  int n3;

};

#endif  // TUPLE4_HPP_
