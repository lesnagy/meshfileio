#ifndef VAL_H_
#define VAL_H_

union val {
  int     ival;
  double  fval;
  char   *sval;
};

typedef union val * val_ptr;

#endif // VAL_H_
