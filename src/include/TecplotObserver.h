#ifndef TECPLOT_OBSERVER_H_
#define TECPLOT_OBSERVER_H_

#include <string>
#include <vector>

class TecplotObserver
{
public:
  inline virtual ~TecplotObserver() {}
  virtual void title       (std::string title)               = 0;
  virtual void variable    (std::string name)                = 0;
  virtual void pointType   (std::string typeName)            = 0; 
  virtual void elementType (std::string typeName)            = 0;
  virtual void vertex      (double  x, double  y, double  z) = 0;
  virtual void field       (double fx, double fy, double fz) = 0;
  virtual void element     (int n0, int n1, int n2, int n3)  = 0;
  virtual void tvalue      (int t)                           = 0;
  virtual void nverts      (int n)                           = 0;
  virtual void nelems      (int e)                           = 0;
  virtual void zone        ()                                = 0;
};

typedef TecplotObserver* TecplotObserverPtr;

#endif  // TECPLOT_OBSERVER_H_
