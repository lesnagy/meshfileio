#ifndef DOLFIN_OBSERVER_H_
#define DOLFIN_OBSERVER_H_

class DolfinObserver
{
public:
  inline virtual ~DolfinObserver() {}
  virtual void vertex  (double x, double y, double z)                       = 0;
  virtual void element (int eidx, int sidx, int n0, int n1, int n2, int n3) = 0;
};

typedef DolfinObserver* DolfinObserverPtr;

#endif  // DOLFIN_OBSERVER_H_
