#ifndef WRITER_H_
#define WRITER_H_

#include <fstream>
#include <iostream>
#include <string>

class Writer
{
public:

  Writer();

  Writer(const std::string & fileName);

  ~Writer();

  std::string fileName() const;

  void fileName(const std::string & fileName);

  void begin();

  void end();

protected:
  std::string   mFileName;
  std::ofstream mFout;
};
  

#endif  // WRITER_H_
