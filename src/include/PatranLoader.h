/**
 * \file   TecplotLoader.h
 * \author L. Nagy
 *
 * Copyright [2016] Lesleis Nagy. All rights reserved.
 */

#ifndef PATRAN_LOADER_H_
#define PATRAN_LOADER_H_

#include <string>

#include "PatranObserver.h"

class PatranLoader
{
public:
  PatranLoader(PatranObserver & observer);
  void load(const std::string & fileName);
private:
  PatranObserver & mObserver;
};

#endif  // PATRAN_LOADER_H_
