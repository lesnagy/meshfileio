#ifndef ELEMENT_INDEXING_H_
#define ELEMENT_INDEXING_H_

enum ElementIndexing
{
  ONE_INDEXING,
  ZERO_INDEXING
};

#endif  // ELEMENT_INDEXING_H_
