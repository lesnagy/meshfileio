#ifndef ELEMENT_TYPE_H_
#define ELEMENT_TYPE_H_

enum ElementType 
{ 
	BAR, 
	TRI, 
	QUAD, 
	TET, 
	WEDGE, 
	HEX
};

#endif  // ELEMENT_INDEXING_H_
