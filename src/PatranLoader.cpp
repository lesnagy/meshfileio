#include "PatranLoader.h"

#include "DebugMacros.h"

#include "patran_bison.h"
#include "patran_flex.h"

void yypat_error(yyscan_t scanner, char const *msg)
{
  std::cout << msg << std::endl;
}

void yypat_error(yyscan_t scanner, PatranObserverPtr msgp, char const *msg)
{
  std::cout << msg << std::endl;
}

PatranLoader::PatranLoader(PatranObserver & observer) :
  mObserver(observer)
{}

void PatranLoader::load(const std::string & fileName)
{
  FILE *myfile = fopen(fileName.c_str(), "r");

  if (!myfile) {
    ERROR("Can't open input file: '" << fileName << "'");
    return;
  }

  yyscan_t myscanner;
  
  if (yypat_lex_init(&myscanner)) {
    ERROR("Error creating parser");
    return;
  }

  do {
    yypat_set_in(myfile, myscanner);
    int parseState = yypat_parse(myscanner, &mObserver);
    DEBUG("parseState: " << parseState);
    if (parseState) {
      DEBUG("Halting parser");
      break;
    } 
  } while (!feof(myfile));
  yypat_lex_destroy(myscanner);
}
