#include <vector>
#include <iostream>
#include <iomanip>

#include "PatranObserver.h"
#include "PatranLoader.h"
#include "PatranWriter.h"

#include "TecplotObserver.h"
#include "TecplotLoader.h"
#include "TecplotWriter.h"

#include "DolfinObserver.h"
#include "DolfinLoader.h"

#include "DebugMacros.h"

///////////////////////////////////////////////////////////////////////////////
// Patran observer type getter                                               //
///////////////////////////////////////////////////////////////////////////////
enum ElementType { BAR, TRI, QUAD, TET, WEDGE, HEX };

class PatranObserverImplType : public PatranObserver
{
public:
  PatranObserverImplType() {}

  ~PatranObserverImplType() {}

  virtual bool vertex(double x, double y, double z) { return false; }

  virtual bool element(int idx, int sid, int n0, int n1, int n2, int n3) { return false; }

  virtual bool elementType(int tid) {
    DEBUG("elementType");
    switch(tid) {
      case 2:
        mElementType = BAR;
        break;
      case 3:
        mElementType = TRI;
        break;
      case 4:
        mElementType = QUAD;
        break;
      case 5:
        mElementType = TET;
        break;
      case 7:
        mElementType = WEDGE;
        break;
      case 8:
        mElementType = HEX;
        break;
    }
    return true;
  }

  ElementType getElementType();

private:
  ElementType mElementType;
};

///////////////////////////////////////////////////////////////////////////////
// Patran observer implementation.                                           //
///////////////////////////////////////////////////////////////////////////////

class PatranObserverImpl : public PatranObserver
{
public:

  PatranObserverImpl(
      std::vector< std::vector<double> > & vertices,
      std::vector< std::vector<int>    > & elements
  ) : mVertices(vertices), mElements(elements)
  {}

  ~PatranObserverImpl() {}

  virtual bool vertex(double x, double y, double z)
  {
    mVertices.push_back({x, y, z});
    return false;
  }

  virtual bool element(int idx, int sidx, int n0, int n1, int n2, int n3)
  {
    mElements.push_back({sidx, n0, n1, n2, n3});
    return false;
  }

  virtual bool elementType(int idx)
  {
    DEBUG("elementType()");
    return false;
  }

private:
  std::vector< std::vector<double> > & mVertices;
  std::vector< std::vector<int>    > & mElements;
};


///////////////////////////////////////////////////////////////////////////////
// Tecplot observer implementation.                                          //
///////////////////////////////////////////////////////////////////////////////

struct Vertex
{
  double x;
  double y;
  double z;
};

struct Field
{
  double x;
  double y;
  double z;
};

struct Element
{
  int n0;
  int n1;
  int n2;
  int n3;
};

class TecplotObserverImpl : public TecplotObserver
{
public:

  TecplotObserverImpl(std::vector<Vertex>      & vert,
                      std::vector<Field>       & field,
                      std::vector<Element>     & elements,
                      std::vector<std::string> & variables) :
    mVert(vert), mField(field), mElements(elements), mVariables(variables),
    mTValue(0), mNZones(0), mTitle(""), mPointType(""), mElementType(""),
    mNVert(0), mNElem(0)
  {}

  ~TecplotObserverImpl() {}

  virtual void title (std::string title)
  {
    std::cout << "TITLE CALLBACK " << title << std::endl;
    mTitle = title;
  }

  virtual void variable (std::string name)
  {
    mVariables.push_back(name);
  }

  virtual void pointType (std::string typeName)
  {
    mPointType = typeName;
  }

  virtual void elementType (std::string typeName)
  {
    mElementType = typeName;
  }

  virtual void vertex (double  x, double  y, double  z)
  {
    mVert.push_back({x, y, z});
  }

  virtual void field (double fx, double fy, double fz)
  {
    mField.push_back({fx, fy, fz});
  }

  virtual void element (int n0, int n1, int n2, int n3)
  {
    mElements.push_back({n0, n1, n2, n3});
  }

  virtual void tvalue (int t)
  {
    mTValue = t;
  }

  virtual void nverts (int n)
  {
    mNVert = n;
  }

  virtual void nelems (int e)
  {
    mNElem = e;
  }

  virtual void zone ()
  {
    mNZones += 1;
  }

  int tvalue () const { return mTValue; }

  int nzones () const { return mNZones; }

  std::string title () const { return mTitle; }

  std::string pointType () const { return mPointType; }

  std::string elementType () const { return mElementType; }

  int nvert () const { return mNVert; }

  int nelem () const { return mNElem; }

private:
  std::vector<Vertex>      & mVert;
  std::vector<Field>       & mField;
  std::vector<Element>     & mElements;
  std::vector<std::string> & mVariables;

  int         mTValue;
  int         mNZones;
  std::string mTitle;
  std::string mPointType;
  std::string mElementType;
  int         mNVert;
  int         mNElem;
};

///////////////////////////////////////////////////////////////////////////////
// Dolfin observer implementation.                                           //
///////////////////////////////////////////////////////////////////////////////

class DolfinObserverImpl : public DolfinObserver
{
public:

  DolfinObserverImpl(
      std::vector< std::vector<double>  > & vertices,
      std::vector< std::vector<int>     > & elements
  ) : mVertices(vertices), mElements(elements)
  {}

  ~DolfinObserverImpl() {}

  virtual void vertex(double x, double y, double z)
  {
    mVertices.push_back({x, y, z});
  }

  virtual void element(int idx, int sidx, int n0, int n1, int n2, int n3)
  {
    mElements.push_back({sidx, n0, n1, n2, n3});
  }
private:
  std::vector< std::vector<double>  > & mVertices;
  std::vector< std::vector<int>     > & mElements;
};

///////////////////////////////////////////////////////////////////////////////
// Main                                                                      //
///////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
  std::vector< std::vector<double> > vert;
  std::vector< std::vector<int>    > elem;

  //////////////
  // Dolfin   //
  //////////////

  DolfinObserverImpl dolfinObserver(vert, elem);
  DolfinLoader dolfinLoader(dolfinObserver);
  //dolfinLoader.load("/home/lesnagy/Install/mmmeshes/multiphase_03/multiphase.xml",
  //                  "/home/lesnagy/Install/mmmeshes/multiphase_03/multiphase_meshfn.xml");

  for (auto v : vert) {
    std::cout 
      << std::right << std::scientific << std::setw(16) << std::setprecision(8) << v[0] << " " 
      << std::right << std::scientific << std::setw(16) << std::setprecision(8) << v[1] << " "
      << std::right << std::scientific << std::setw(16) << std::setprecision(8) << v[2]
      << std::endl;
  }

  for (auto e : elem) {
    std::cout 
      << "[" << std::left << std::setw(5) << e[0] << "] "
      << std::right << std::setw(5) << e[1] << " "
      << std::right << std::setw(5) << e[2] << " "
      << std::right << std::setw(5) << e[3] << " "
      << std::right << std::setw(5) << e[4]
      << std::endl;
  }

  PatranObserverImplType patObserver;
  PatranLoader patLoader(patObserver);
  patLoader.load("../../testdataset/patran/cube02/cube02.pat");

  switch (patObserver.getElementType()) {
    case BAR:
      std::cout << "Bar" << std::endl;
      break;
    case TRI:
      std::cout << "Tri" << std::endl;
      break;
    case QUAD:
      std::cout << "Quad" << std::endl;
      break;
    case TET:
      std::cout << "Tetra" << std::endl;
      break;
    case WEDGE:
      std::cout << "Wedge" << std::endl;
      break;
    case HEX:
      std::cout << "Hex" << std::endl;
      break;
  }

  PatranObserverImpl patObserver3d(vert, elem);
  PatranLoader patLoader3D(patObserver3d);
  patLoader3D.load("../../testdataset/patran/cube02/cube02.pat");

  return 0;
}