#include <ctime>
#include <algorithm>
#include <iomanip>

#include "PatranWriter.h"

PatranWriter::PatranWriter() : Writer("") 
{}

PatranWriter::PatranWriter(const std::string & fileName) : Writer(fileName)
{}

PatranWriter::~PatranWriter()
{}

void PatranWriter::writeTitle(const std::string & title)
{
  mFout << "25" << std::right << std::setw(8) << 0 
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 1
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::endl;
  mFout << title << std::endl;
};

void PatranWriter::writeHeader(int nvert, int nelem)
{
  time_t now = time(0);
  tm *gmt = gmtime(&now);

  mFout << "26" << std::right << std::setw(8) << 0 
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 1
                << std::right << std::setw(8) << nvert
                << std::right << std::setw(8) << nelem
                << std::right << std::setw(8) << 1
                << std::right << std::setw(8) << 4
                << std::right << std::setw(8) << 0
                << std::endl;
  mFout << gmt->tm_mon  << "/" << gmt->tm_mday << "/" << gmt->tm_year << "   "
        << gmt->tm_hour << ":" << gmt->tm_min  << ":" << gmt->tm_sec  << "   "
        << "5.0" << std::endl;
}

void PatranWriter::writeVertex(int id, double x, double y, double z)
{
  mFout << "01" << std::right << std::setw(8) << id
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 2
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::endl;

  mFout
    << std::right << std::scientific << std::setw(16) << std::setprecision(8) << x
    << std::right << std::scientific << std::setw(16) << std::setprecision(8) << y
    << std::right << std::scientific << std::setw(16) << std::setprecision(8) << z
    << std::endl;

  mFout << "0G       6       0       0  000000" << std::endl;
}

void PatranWriter::writeElement(int id, int sid, 
                                int n0, int n1, int n2, int n3)
{
  mFout << "02" << std::right << std::setw(8) << id
                << std::right << std::setw(8) << 5
                << std::right << std::setw(8) << 2
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::endl;

  mFout << std::right << std::setw(8) << 4
        << std::right << std::setw(8) << 0
        << std::right << std::setw(8) << sid
        << std::right << std::setw(8) << 0
        << " " << std::right << std::setw(11) << std::setprecision(10) << 0.0
        << " " << std::right << std::setw(11) << std::setprecision(10) << 0.0
        << " " << std::right << std::setw(11) << std::setprecision(10) << 0.0
        << std::endl;

  mFout << std::right << std::setw(8) << n0
        << std::right << std::setw(8) << n1
        << std::right << std::setw(8) << n2
        << std::right << std::setw(8) << n3
        << std::endl;
}

void PatranWriter::writeElement(int id, int sid, 
                                int n0, int n1, int n2)
{
  mFout << "02" << std::right << std::setw(8) << id
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 2
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::endl;

  mFout << std::right << std::setw(8) << 4
        << std::right << std::setw(8) << 0
        << std::right << std::setw(8) << sid
        << std::right << std::setw(8) << 0
        << " " << std::right << std::setw(11) << std::setprecision(10) << 0.0
        << " " << std::right << std::setw(11) << std::setprecision(10) << 0.0
        << " " << std::right << std::setw(11) << std::setprecision(10) << 0.0
        << std::endl;

  mFout << std::right << std::setw(8) << n0
        << std::right << std::setw(8) << n1
        << std::right << std::setw(8) << n2
        << std::endl;
}

void PatranWriter::writeMaterialProperties(const std::vector<double> & values)
{
  mFout << "03" << std::right << std::setw(8) << 1
                << std::right << std::setw(8) << 1
                << std::right << std::setw(8) << 20
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::endl;

  // The specification tells us to expect 96 floating point numbers, so lets
  // make sure we have them
  std::vector<double> vals(96);

  if (values.size() <= vals.size()) {
    for (size_t i = 0; i < values.size(); ++i) {
      vals[i] = values[i];
    }
  } else {
    for (size_t i = 0; i < vals.size(); ++i) {
      vals[i] = values[i];
    }
  }

  for (size_t i = 0; i < 95; i += 5) {
    mFout 
      << "  " << std::right << std::scientific << std::setw(11) << std::setprecision(10) << vals[i+0]
      << "  "  << std::right << std::scientific << std::setw(11) << std::setprecision(10) << vals[i+1]
      << "  "  << std::right << std::scientific << std::setw(11) << std::setprecision(10) << vals[i+2]
      << "  "  << std::right << std::scientific << std::setw(11) << std::setprecision(10) << vals[i+3]
      << "  "  << std::right << std::scientific << std::setw(11) << std::setprecision(10) << vals[i+4]
      << std::endl;
  }
  mFout 
    << "  " << std::right << std::scientific << std::setw(11) << std::setprecision(6) << vals[94]
    << std::endl;
}

void PatranWriter::writeElementProperties(int sid, double value)
{
  mFout << "04" << std::right << std::setw(8) << sid
                << std::right << std::setw(8) << 1
                << std::right << std::setw(8) << 1
                << std::right << std::setw(8) << 8
                << std::right << std::setw(8) << 8
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 1
                << std::right << std::setw(8) << 0
                << std::endl;

  mFout 
    << "  " << std::right << std::scientific << std::setw(11) << std::setprecision(6) << value
    << std::endl;
}

void PatranWriter::writeEnd()
{
  mFout << "99" << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 1
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::right << std::setw(8) << 0
                << std::endl;
}

