#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "DolfinLoader.h"

#include "DebugMacros.h"

#include "rapidxml.hpp"

DolfinLoader::DolfinLoader(DolfinObserver & observer) :
  mObserver(observer)
{}

void DolfinLoader::load(const std::string & meshFileName,
                        const std::string & meshFunctionFileName)
{
  DEBUG("DolfinLoader::load()");
  using namespace std;
  using namespace rapidxml;

  //////////////////////////////////////////////////////////////////////////////
  // Retrieve data mesh function xml.                                         //
  //////////////////////////////////////////////////////////////////////////////

  initSubmeshIds(meshFunctionFileName);

  //////////////////////////////////////////////////////////////////////////////
  // Retrieve data mesh xml.                                                  //
  //////////////////////////////////////////////////////////////////////////////

  std::ifstream finmesh(meshFileName);
  if (!finmesh.good()) {
    ERROR("Could not find file '" << meshFileName << "'");
    return;
  }
  vector<char> bufmesh((istreambuf_iterator<char>(finmesh)),
                        istreambuf_iterator<char>());
  bufmesh.push_back('\0');
  xml_document<> docmesh;
  docmesh.parse<0>(&bufmesh[0]);
  DEBUG("Parsed mesh xml");

  //////////////////////////////////////////////////////////////////////////////
  // Parse it                                                                 //
  //////////////////////////////////////////////////////////////////////////////

  xml_node<> *dolfin_node = docmesh.first_node("dolfin");
  DEBUG("Parsed tag: 'dolfin'");

  xml_node<> *mesh_node = dolfin_node->first_node("mesh");
  std::string celltype_attr = mesh_node->first_attribute("celltype")->value();
  std::string dim_attr = mesh_node->first_attribute("dim")->value();
  DEBUG("Parsed tag: 'mesh'");
  DEBUG("  attribute 'celltype' : " << celltype_attr);
  DEBUG("  attribute 'dim'      : " << dim_attr);

  xml_node<> *vertices_node = mesh_node->first_node("vertices");
  int vertices_size_attr = atoi(vertices_node->first_attribute("size")->value());
  DEBUG("Parsed tag: 'vertices'");
  DEBUG("  attribute 'size'     : " << vertices_size_attr);

  for (xml_node<> * vertex_node = vertices_node->first_node("vertex"); 
       vertex_node;
       vertex_node = vertex_node->next_sibling())
  {
    int index_attr = atoi(vertex_node->first_attribute("index")->value());
    double x_attr  = atof(vertex_node->first_attribute("x")->value());
    double y_attr  = atof(vertex_node->first_attribute("y")->value());
    double z_attr  = atof(vertex_node->first_attribute("z")->value());
    DEBUG("Parsed tag: 'vertex'");
    DEBUG("  attribute 'index'     : " << index_attr);
    DEBUG("  attribute 'x'         : " << x_attr);
    DEBUG("  attribute 'y'         : " << y_attr);
    DEBUG("  attribute 'z'         : " << z_attr);
    mObserver.vertex(x_attr, y_attr, z_attr);
  }

  xml_node<> * cells_node = mesh_node->first_node("cells");
  int cells_size_attr = atoi(cells_node->first_attribute("size")->value());
  DEBUG("Parsed tag: 'cells'");
  DEBUG("  attribute 'size'   : " << cells_size_attr);


  DEBUG(" size of mSubmeshIds: " << mSubmeshIds.size());
  for (xml_node<> * tetrahedron_node = cells_node->first_node("tetrahedron");
       tetrahedron_node;
       tetrahedron_node = tetrahedron_node->next_sibling())
  {
    int index_attr = atoi(tetrahedron_node->first_attribute("index")->value());
    int v0_attr    = atoi(tetrahedron_node->first_attribute("v0")->value());
    int v1_attr    = atoi(tetrahedron_node->first_attribute("v1")->value());
    int v2_attr    = atoi(tetrahedron_node->first_attribute("v2")->value());
    int v3_attr    = atoi(tetrahedron_node->first_attribute("v3")->value());
    int sid        = mSubmeshIds[index_attr];
    DEBUG("Parsed tag: 'tetrahedron'");
    DEBUG("  attribute index: " << index_attr);
    DEBUG("  attribute v0   : " << v0_attr);
    DEBUG("  attribute v1   : " << v1_attr);
    DEBUG("  attribute v2   : " << v2_attr);
    DEBUG("  attribute v3   : " << v3_attr);
    DEBUG("         (sid)   : " << sid);
    mObserver.element(index_attr, sid, v0_attr, v1_attr, v2_attr, v3_attr);
  }

}

void DolfinLoader::initSubmeshIds(const std::string & meshFunctionFileName)
{ 
  DEBUG("DolfinLoader::initSubmeshIds()");
  using namespace std;
  using namespace rapidxml;

  mSubmeshIds.clear();

  std::ifstream finmeshfun(meshFunctionFileName);
  if (!finmeshfun.good()) {
    ERROR("Could not find file '" << meshFunctionFileName << "'");
    return;
  }
  vector<char> bufmeshfun((istreambuf_iterator<char>(finmeshfun)),
                           istreambuf_iterator<char>());
  bufmeshfun.push_back('\0');
  xml_document<> docmeshfun;
  docmeshfun.parse<0>(&bufmeshfun[0]);
  DEBUG("Parsed mesh function xml");

  xml_node<> *dolfin_node = docmeshfun.first_node("dolfin");
  DEBUG("Parsed tag: 'dolfin'");

  xml_node<> *mesh_function_node = dolfin_node->first_node("mesh_function");
  DEBUG("Parsed tag: 'meshfunction'");

  xml_node<> *mesh_value_collection_node = 
    mesh_function_node->first_node("mesh_value_collection");

  std::string name_attr = 
    mesh_value_collection_node->first_attribute("name")->value();
  std::string type_attr = 
    mesh_value_collection_node->first_attribute("type")->value();
  int dim_attr = 
    atoi(mesh_value_collection_node->first_attribute("dim")->value());
  int size_attr = 
    atoi(mesh_value_collection_node->first_attribute("size")->value());
  
  DEBUG("Parsed tag: 'mesh_value_collection'");
  DEBUG("  attribute 'name'     : " << name_attr);
  DEBUG("  attribute 'type'     : " << type_attr);
  DEBUG("  attribute 'dim'      : " << dim_attr);
  DEBUG("  attribute 'size'     : " << size_attr);

  mSubmeshIds.resize(size_attr);

  for (xml_node<> * value_node = mesh_value_collection_node->first_node("value");
       value_node;
       value_node = value_node->next_sibling())
  {
    int cell_index_attr = 
      atoi(value_node->first_attribute("cell_index")->value());
    int local_entity_attr = 
      atoi(value_node->first_attribute("local_entity")->value());
    int value_attr = 
      atoi(value_node->first_attribute("value")->value());
    
    DEBUG("Parsed tag: 'value'");
    DEBUG("  attribute 'cell_index'   : " << cell_index_attr);
    DEBUG("  attribute 'local_entity' : " << local_entity_attr);
    DEBUG("  attribute 'value'        : " << value_attr);
    mSubmeshIds[cell_index_attr] = value_attr;
  }
}