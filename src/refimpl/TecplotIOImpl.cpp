#include "refimpl/TecplotIOImpl.h"

#include "TecplotLoader.h"
#include "TecplotWriter.h"

TecplotObserverImpl::TecplotObserverImpl(std::vector<Vertex3D>    & vert,
                                         std::vector<Field3D>     & field,
                                         std::vector<Tuple4>      & elements,
                                         std::vector<std::string> & variables):
    mVert        (vert), mField(field), mElements(elements), mVariables(variables),
    mTValue      (0),    mNZones(0),    mTitle(""),          mPointType(""), 
    mElementType (""),   mNVert(0),     mNElem(0)
{}

TecplotObserverImpl:: ~TecplotObserverImpl() 
{}

void TecplotObserverImpl::title (std::string title)
{
  mTitle = title;
}

void TecplotObserverImpl::variable (std::string name)
{
  mVariables.push_back(name);
}

void TecplotObserverImpl::pointType (std::string typeName)
{
  mPointType = typeName;
}

void TecplotObserverImpl::elementType (std::string typeName)
{
  mElementType = typeName;
}

void TecplotObserverImpl::vertex (double  x, double  y, double  z)
{
  mVert.push_back({x, y, z});
}

void TecplotObserverImpl::field (double fx, double fy, double fz)
{
  mField.push_back({fx, fy, fz});
}

void TecplotObserverImpl::element (int n0, int n1, int n2, int n3)
{
  mElements.push_back({n0, n1, n2, n3});
}

void TecplotObserverImpl::tvalue (int t)
{
  mTValue = t;
}

void TecplotObserverImpl::nverts (int n)
{
  mNVert = n;
}

void TecplotObserverImpl::nelems (int e)
{
  mNElem = e;
}

void TecplotObserverImpl::zone ()
{
  mNZones += 1;
}

int TecplotObserverImpl::tvalue () const 
{ 
  return mTValue;
}

int TecplotObserverImpl::nzones () const 
{ 
  return mNZones;
}

std::string TecplotObserverImpl::title () const 
{ 
  return mTitle;
}

std::string TecplotObserverImpl::pointType () const 
{ 
  return mPointType;
}

std::string TecplotObserverImpl::elementType () const 
{ 
  return mElementType;
}

int TecplotObserverImpl::nvert () const 
{ 
  return mNVert;
}

int TecplotObserverImpl::nelem () const 
{ 
  return mNElem;
}

void loadFromTecplot(const std::string        & fileName,
                     std::vector<Vertex3D>    & vert,
                     std::vector<Field3D>     & field,
                     std::vector<Tuple4>      & elements,
                     std::vector<std::string> & variables,
                     int                      * nzones)
{
  TecplotObserverImpl tecObserver(vert, field, elements, variables);
  TecplotLoader tecLoader(tecObserver);
  tecLoader.load(fileName);
  *nzones = tecObserver.nzones();
}

void saveToTecplot(const std::string          & fileName,
                     std::vector<Vertex3D>    & vert,
                     std::vector<Field3D>     & field,
                     std::vector<Tuple4>      & elem,
                     std::vector<std::string> & vars,
                     int                        nzones)
{
  TecplotWriter tecWriter(fileName);
  tecWriter.begin();
  tecWriter.writeTitle("Exported using meshfileio");
  tecWriter.writeVariables(vars);
  tecWriter.writeZone(1, vert.size(), elem.size(), 
                      "FEPOINT", "TETRAHEDRON");

  for(size_t i = 0; i < vert.size(); ++i) {
    tecWriter.writeVertexField(vert[i].x,  vert[i].y,  vert[i].z, 
                               field[i].x, field[i].y, field[i].z);
  }

  for(size_t i = 0; i < elem.size(); ++i) {
    tecWriter.writeElement(elem[i].n0, elem[i].n1, elem[i].n2, elem[i].n3);
  }

  if (field.size() > vert.size()) {
    for (size_t zone = 1; zone < field.size()/vert.size(); ++zone) {
      tecWriter.writeZone(zone+1, vert.size(), elem.size(), 
                          "FEPOINT", "TETRAHEDRON",
                          1,3,1,1);
      for (size_t i = zone*vert.size(); i < zone*vert.size() + vert.size(); ++i) {
        tecWriter.writeField(field[i].x, field[i].y, field[i].z);
      }
    }
  }

  tecWriter.end();
}
