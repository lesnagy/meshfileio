#include <set>

#include "refimpl/PatranIOImpl.h"

#include "PatranLoader.h"
#include "PatranWriter.h"

///////////////////////////////////////////////////////////////////////////////
// PatranObserverImplType                                                    //
///////////////////////////////////////////////////////////////////////////////

PatranObserverImplType::PatranObserverImplType()
{}

PatranObserverImplType::~PatranObserverImplType()
{}

bool PatranObserverImplType::vertex(double x, double y, double z)
{
  // Don't stop parsing if we hit a vertex.
  return false;
}

bool PatranObserverImplType::element(int idx, int sid, int n0, int n1, int n2, int n3)
{
  // Don't stop parsing if we hit an element.
  return false;
}

bool PatranObserverImplType::elementType(int tid)
{
  switch(tid) {
    case 2:
      mElementType = BAR;
      break;
    case 3:
      mElementType = TRI;
      break;
    case 4:
      mElementType = QUAD;
      break;
    case 5:
      mElementType = TET;
      break;
    case 7:
      mElementType = WEDGE;
      break;
    case 8:
      mElementType = HEX;
      break;
  }
  // *DO* stop parsing if we find an element type.
  return true;
}

ElementType PatranObserverImplType::getElementType()
{
  return mElementType;
}

ElementType getPatranMeshType(const std::string & fileName)
{
  PatranObserverImplType observer;
  PatranLoader loader(observer);
  loader.load(fileName);
  return observer.getElementType();
}

///////////////////////////////////////////////////////////////////////////////
// PatranLoader2D                                                            //
///////////////////////////////////////////////////////////////////////////////

PatranObserverImpl2D::PatranObserverImpl2D(std::vector<Vertex2D> & vertices,
                                           std::vector<Tuple3>   & elements,
                                           ElementIndexing         indexing) 
: mVertices(vertices), mElements(elements), mIndexing(indexing)
{}

PatranObserverImpl2D::~PatranObserverImpl2D() 
{}

bool PatranObserverImpl2D::vertex(double x, double y, double z)
{
  mVertices.push_back({x, y});

  // Don't exit on vertex.
  return false; 
}

bool PatranObserverImpl2D::element(int idx, int sid, 
                                   int n0, int n1, int n2, int n3)
{
  switch (mIndexing) {
    case ONE_INDEXING:
      mElements.push_back({sid, n0-1, n1-1, n2-1});
      break;
    case ZERO_INDEXING:
      mElements.push_back({sid, n0, n1, n2});
      break;
  }

  // Don't stop parsing on element.
  return false;
}

bool PatranObserverImpl2D::elementType(int tid)
{
  // Don't stop parsing when we find an element type.
  return false;
}

void loadFromPatran2D(const std::string     & fileName,
                      std::vector<Vertex2D> & vertices,
                      std::vector<Tuple3>   & elements,
                      ElementIndexing         indexing)
{
  vertices.clear();
  elements.clear();

  PatranObserverImpl2D observer(vertices, elements, indexing);
  PatranLoader loader(observer);
  loader.load(fileName);
}

void saveToPatran2D(const std::string     & fileName,
                    std::vector<Vertex2D> & vertices,
                    std::vector<Tuple3>   & elements,
                    ElementIndexing         indexing)
{
  int id = 0;

  PatranWriter writer(fileName);

  writer.begin();
  writer.writeTitle("Exported using meshfileio");
  writer.writeHeader(vertices.size(), elements.size());

  switch (indexing) {
    case ZERO_INDEXING:
      id = 0;
      break;
    case ONE_INDEXING:
      id = 1;
      break;
  }
  for (auto v : vertices) {
    writer.writeVertex(id, v.x, v.y, 0.0);
    id++;
  }

  switch (indexing) {
    case ZERO_INDEXING:
      id = 0;
      break;
    case ONE_INDEXING:
      id = 1;
      break;
  }
  std::set<int> sids;

  switch (indexing) {
    case ZERO_INDEXING:
      for (auto e : elements) {
        sids.insert(e.sid);
        writer.writeElement(id, e.sid, e.n0, e.n1, e.n2);
        id++;
      }
      break;
    case ONE_INDEXING:
      for (auto e : elements) {
        sids.insert(e.sid);
        writer.writeElement(id, e.sid, e.n0+1, e.n1+1, e.n2+1);
        id++;
      }
      break;
  }

  std::vector<double> values = { 
    0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  writer.writeMaterialProperties(values);

  for (int sid : sids) {
    writer.writeElementProperties(sid, 1.0);
  }

  writer.writeEnd();

  writer.end();
}

///////////////////////////////////////////////////////////////////////////////
// PatranObserverImpl3D                                                      //
///////////////////////////////////////////////////////////////////////////////

PatranObserverImpl3D::PatranObserverImpl3D(std::vector<Vertex3D> & vertices,
                                           std::vector<Tuple4>   & elements,
                                           ElementIndexing         indexing) 
: mVertices(vertices), mElements(elements), mIndexing(indexing)
{}

PatranObserverImpl3D::~PatranObserverImpl3D() 
{}

bool PatranObserverImpl3D::vertex(double x, double y, double z)
{
  mVertices.push_back({x, y, z});

  // Don't stop parsing if we find a vertex.
  return false;
}

bool PatranObserverImpl3D::element(int idx, int sid, 
                                   int n0, int n1, int n2, int n3)
{
  switch (mIndexing) {
    case ONE_INDEXING:
      mElements.push_back({sid, n0-1, n1-1, n2-1, n3-1});
      break;
    case ZERO_INDEXING:
      mElements.push_back({sid, n0, n1, n2, n3});
      break;
  }

  // Don't stop parsing if we find an element.
  return false;
}

bool PatranObserverImpl3D::elementType(int tid)
{
  // Don't stop parsing if we find an element type.
  return false;
}

void loadFromPatran3D(const std::string   & fileName,
                    std::vector<Vertex3D> & vertices,
                    std::vector<Tuple4>   & elements,
                    ElementIndexing         indexing)
{
  vertices.clear();
  elements.clear();

  PatranObserverImpl3D observer(vertices, elements, indexing);
  PatranLoader loader(observer);
  loader.load(fileName);
}

void saveToPatran3D(const std::string     & fileName,
                    std::vector<Vertex3D> & vertices,
                    std::vector<Tuple4>   & elements,
                    ElementIndexing         indexing)
{
  int id = 0;

  PatranWriter writer(fileName);

  writer.begin();
  writer.writeTitle("Exported using meshfileio");
  writer.writeHeader(vertices.size(), elements.size());

  switch (indexing) {
    case ZERO_INDEXING:
      id = 0;
      break;
    case ONE_INDEXING:
      id = 1;
      break;
  }
  for (auto v : vertices) {
    writer.writeVertex(id, v.x, v.y, v.z);
    id++;
  }

  switch (indexing) {
    case ZERO_INDEXING:
      id = 0;
      break;
    case ONE_INDEXING:
      id = 1;
      break;
  }
  std::set<int> sids;
  switch (indexing) {
    case ZERO_INDEXING:
      for (auto e : elements) {
        sids.insert(e.sid);
        writer.writeElement(id, e.sid, e.n0, e.n1, e.n2, e.n3);
        id++;
      }
      break;
    case ONE_INDEXING:
      for (auto e : elements) {
        sids.insert(e.sid);
        writer.writeElement(id, e.sid, e.n0+1, e.n1+1, e.n2+1, e.n3+1);
        id++;
      }
      break;
  }

  std::vector<double> values = { 
    0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  writer.writeMaterialProperties(values);

  for (int sid : sids) {
    writer.writeElementProperties(sid, 1.0);
  }

  writer.writeEnd();

  writer.end();
}
