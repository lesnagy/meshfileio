#include "refimpl/DolfinIOImpl.h"

///////////////////////////////////////////////////////////////////////////////
// DolfinObserverImpl3D                                                      //
///////////////////////////////////////////////////////////////////////////////

DolfinObserverImpl3D::DolfinObserverImpl3D(std::vector<Vertex3D> & vertices,
                                           std::vector<Tuple4>   & elements,
                                           ElementIndexing         indexing)
: mVertices(vertices), mElements(elements), mIndexing(indexing)
{}

DolfinObserverImpl3D::~DolfinObserverImpl3D()
{}

void DolfinObserverImpl3D::vertex(double x, double y, double z)
{
  mVertices.push_back({x, y, z});
}

void DolfinObserverImpl3D::element(int idx, int sid, 
                                   int n0, int n1, int n2, int n3)
{
  switch (mIndexing) {
    case ONE_INDEXING:
      mElements.push_back({sid, n0-1, n1-1, n2-1, n3-1});
      break;
    case ZERO_INDEXING:
      mElements.push_back({sid, n0, n1, n2, n3});
      break;
  }
}

void loadFromDolfin3D(const std::string     & fileName,
                      const std::string     & meshFunFileName,
                      std::vector<Vertex3D> & vertices,
                      std::vector<Tuple4>   & elements,
                      ElementIndexing         indexing)
{
  vertices.clear();
  elements.clear();

  DolfinObserverImpl3D observer(vertices, elements, indexing);
  DolfinLoader loader(observer);
  loader.load(fileName, meshFunFileName);
}

void saveToDolfin3D(const std::string     & fileName,
                    const std::string     & meshFunFileName,
                    std::vector<Vertex3D> & vertices,
                    std::vector<Tuple4>   & elements,
                    ElementIndexing         indexing)
{}

