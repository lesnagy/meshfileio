%define api.pure full
%lex-param   { yyscan_t scanner }
%parse-param { yyscan_t scanner }
%parse-param { TecplotObserverPtr observer_ptr }
%define api.prefix {yytec_}
%define parse.trace

%code requires {

#include <algorithm>
#include <cstdio>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "DebugMacros.h"
#include "TecplotObserver.h"

#include "tecplot_bison.h"
#include "val.h"

#ifdef YYSTYPE
#  undef YYSTYPE
#  define YYSTYPE val
#else
#  define YYSTYPE val
#endif

#ifndef YY_TYPEDEF_YY_SCANNER_T
#  define YY_TYPEDEF_YY_SCANNER_T
   typedef void* yyscan_t;
#endif

#ifdef YYTEC_DEBUG
#  undef YYTEC_DEBUG
#  define YYTEC_DEBUG 1
#else
#  define YYTEC_DEBUG 1
#endif

int yylex (YYSTYPE * yylval_param ,yyscan_t yyscanner);
void yyerror(yyscan_t scanner, char const *msg);
void yyerror(yyscan_t scanner, TecplotObserverPtr observer_ptr, char const *msg);

}

// Define constant string tokens
%token TITLE
%token VARIABLES
%token ZONE
%token FEPOINT
%token TETRAHEDRON
%token VARSHARELIST
%token CONNECTIVITYSHAREZONE
%token EQUAL_SYMBOL
%token COMMA_SYMBOL
%token T_SYMBOL
%token N_SYMBOL
%token E_SYMBOL
%token F_SYMBOL
%token ET_SYMBOL
%token LPAREN_SYMBOL
%token RPAREN_SYMBOL
%token RANGE
%token STRING_LITERAL
%token FLOAT
%token INT
%token ENDL

%%

// This is the actual gramar that bison will parse.

file: 
        title_line variables_line zones
        ;

title_line: 
        TITLE EQUAL_SYMBOL STRING_LITERAL ENDL
        {
          DEBUG("title_line: " << $3.sval);
          observer_ptr->title($3.sval);
          free($3.sval);
        }
        ;




variables_line:
        VARIABLES EQUAL_SYMBOL variables_list ENDL
        ;

variables_list:
        STRING_LITERAL
        {
          DEBUG("variables_list: " << $1.sval);
          observer_ptr->variable($1.sval);
          free($1.sval);
        }
        | variables_list COMMA_SYMBOL STRING_LITERAL
        {
          DEBUG("variables_list: " << $3.sval);
          observer_ptr->variable($3.sval);
          free($3.sval);
        }
        ;




zones:
        zone_data
        | zones zone_data
        ;

zone_data: 
        zone_line verts_and_fields elements
        | zone_line fields
        ;

zone_line:
        ZONE zone_t zone_nverts COMMA_SYMBOL zone_nelems zone_fepoint 
                                COMMA_SYMBOL zone_elem_type ENDL
        {
          DEBUG("Zone line type 1");
          observer_ptr->zone();
        }
        | ZONE zone_t zone_nverts COMMA_SYMBOL zone_nelems ENDL 
          zone_fepoint COMMA_SYMBOL zone_elem_type ENDL
        {
          DEBUG("Zone line type 2");
          observer_ptr->zone();
        }
        | ZONE zone_t zone_nverts COMMA_SYMBOL zone_nelems ENDL 
          zone_fepoint COMMA_SYMBOL zone_elem_type COMMA_SYMBOL
          zone_varsharelist COMMA_SYMBOL zone_connectivitysharezone ENDL
        {
          DEBUG("Zone line type 3");
          observer_ptr->zone();
        }
        ;

zone_t:
        T_SYMBOL EQUAL_SYMBOL STRING_LITERAL
        {
          DEBUG("t: " << $3.sval);
          std::string s($3.sval);
          s.erase(std::remove(s.begin(), s.end(), '"'), s.end());
          s.erase(std::remove(s.begin(), s.end(), ' '), s.end());
          observer_ptr->tvalue(atoi(s.c_str()));
          free($3.sval);
        }
        ;

zone_nverts:
        N_SYMBOL EQUAL_SYMBOL INT
        {
          DEBUG("nverts = " << $3.ival);
          observer_ptr->nverts($3.ival);
        }
        ;

zone_nelems:
        E_SYMBOL EQUAL_SYMBOL INT
        {
          DEBUG("nelems = " << $3.ival);
          observer_ptr->nelems($3.ival);
        }
        ;

zone_fepoint:
        F_SYMBOL EQUAL_SYMBOL FEPOINT
        {
          DEBUG("finite element point FEPOINT");
          observer_ptr->pointType("FEPOINT");
        }
        ;

zone_elem_type:
        ET_SYMBOL EQUAL_SYMBOL TETRAHEDRON
        {
          DEBUG("finite element TETRAHEDRON");
          observer_ptr->elementType("TETRAHEDRON");
        }
        ;

zone_varsharelist:
        VARSHARELIST EQUAL_SYMBOL 
        LPAREN_SYMBOL RANGE EQUAL_SYMBOL INT RPAREN_SYMBOL
        {
          DEBUG("varshare list");
        }
        ;

zone_connectivitysharezone:
        CONNECTIVITYSHAREZONE EQUAL_SYMBOL INT
        {
          DEBUG("connectivity share zone");
        }
        ;

verts_and_fields:
        FLOAT FLOAT FLOAT FLOAT FLOAT FLOAT ENDL
        {
          DEBUG($1.fval << ", " << $2.fval << ", " << $3.fval << ", "
             << $4.fval << ", " << $5.fval << ", " << $6.fval);
          observer_ptr->vertex($1.fval, $2.fval, $3.fval);
          observer_ptr->field( $4.fval, $5.fval, $6.fval);
        }
        | verts_and_fields FLOAT FLOAT FLOAT FLOAT FLOAT FLOAT ENDL
        {
          DEBUG($2.fval << ", " << $3.fval << ", " << $4.fval << ", "
             << $5.fval << ", " << $6.fval << ", " << $7.fval);
          observer_ptr->vertex($2.fval, $3.fval, $4.fval);
          observer_ptr->field( $5.fval, $6.fval, $7.fval);
        }

fields:
        FLOAT FLOAT FLOAT ENDL
        {
          DEBUG($1.fval << ", " << $2.fval << ", " << $3.fval);
          observer_ptr->field($1.fval, $2.fval, $3.fval);
        }
        | fields FLOAT FLOAT FLOAT ENDL
        {
          DEBUG($2.fval << ", " << $3.fval << ", " << $4.fval);
          observer_ptr->field($2.fval, $3.fval, $4.fval);
        }

elements:
       INT INT INT INT ENDL
       {
          DEBUG($1.ival << ", " << $2.ival << ", " << $3.ival << ", " << $4.ival);
          observer_ptr->element($1.ival, $2.ival, $3.ival, $4.ival);
       }
       | elements INT INT INT INT ENDL
       {
          DEBUG($2.ival << ", " << $3.ival << ", " << $4.ival << ", " << $5.ival);
          observer_ptr->element($2.ival, $3.ival, $4.ival, $5.ival);
       }

%%

