%define api.pure full
%lex-param   { yyscan_t scanner }
%parse-param { yyscan_t scanner }
%parse-param { PatranObserverPtr observer_ptr }
%define api.prefix {yypat_}

%code requires {

#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <iomanip>

#include "PatranObserver.h"
#include "DebugMacros.h"

#include "patran_bison.h"
#include "val.h"

#ifdef YYSTYPE
#  undef YYSTYPE
#  define YYSTYPE val
#else
#  define YYSTYPE val
#endif

#ifndef YY_TYPEDEF_YY_SCANNER_T
#  define YY_TYPEDEF_YY_SCANNER_T
   typedef void* yyscan_t;
#endif

#ifdef YYPAT_DEBUG
#  undef YYPAT_DEBUG
#  define YYPAT_DEBUG 1
#else
#  define YYPAT_DEBUG 1
#endif

int yylex (YYSTYPE * yylval_param, yyscan_t yyscanner);
void yyerror(yyscan_t scanner, char const *msg);
void yyerror(yyscan_t scanner, PatranObserverPtr observer_ptr, char const *msg);

}

// Define constant string tokens
%token NODE_CARD_CONSTRAINTS_ID
%token ENDL

// Define the 'terminal symbol' token types I'm going to use (in CAPS by 
// convention) and associate each with a field of the union.
%token  INT
%token  FLOAT
%token  STRING
%token  TIME
%token  VERSION
%token  DATE

%%

// This is the actual gramar that bison will parse.

file: 
          title 
          summary 
          nodes 
          elements 
          material_properties 
          element_properties 
          element_loads
          {
            DEBUG("file");
          }

//file: 
//        title summary nodes elements 

start_card:
        INT INT INT INT INT INT INT INT INT
        {
          int v1 = $1.ival;
          int v2 = $2.ival;
          int v3 = $3.ival;
          int v4 = $4.ival;
          int v5 = $5.ival;
          int v6 = $6.ival;
          int v7 = $7.ival;
          int v8 = $8.ival;
          int v9 = $9.ival;

          DEBUG("start_card: " << v1 << ", " << v2 << ", " << v3 << ", " 
                  << v4 << ", " << v5 << ", " << v6 << ", " << v7 << ", " 
                  << v8 << ", " << v9);
          
          if (v1 == 25) {
            // Summary data
            int nelem = $5.ival;
            int nvert = $6.ival;
          }

          if (v1 == 1) {
            // Node data, do nothing
          }

          if (v1 == 2) {
            // Element data
            bool stopParsing = observer_ptr->elementType(v3);
            if (stopParsing) {
              DEBUG("Stopping parse");
              return 1;
            }

            $$ = $2;
          }

          if (v1 == 3) {
            // Material properties (call it a halt here)
            DEBUG("Stopping parse");
            return 1;
          }

        }
        ;




title:
        start_card ENDL title_data ENDL
        {
          DEBUG("title");
        }
        ;


title_data:
        title_string_component title_data
        | title_string_component
        {
          DEBUG("title_data");
        }
        ;

title_string_component:
        STRING 
        {
          const char* v1 = $1.sval;
          DEBUG("title_string_component (STRING): " << v1);
          free($1.sval);
        }
        | FLOAT 
        {
          double v1 = $1.fval;
          DEBUG("title_string_component (FLOAT): " << v1);
        }
        | INT 
        {
          int  v1 = $1.ival;
          DEBUG("title_string_component (INT): " << v1);
        }
        | TIME 
        {
          const char*  v1 = $1.sval;
          DEBUG("title_string_component (TIME): " << v1);
          free($1.sval);
        }
        | DATE 
        {
          const char*  v1 = $1.sval;
          DEBUG("title_string_component (DATE): " << v1);
          free($1.sval);
        }
        | VERSION
        {
          const char*  v1 = $1.sval;
          DEBUG("title_string_component (VERSION): " << v1);
          free($1.sval);
        }
        ;



summary:
        start_card ENDL DATE TIME FLOAT ENDL
        {
          DEBUG("summary");
        }
        ;




nodes:  node 
        | nodes node
        ;

node:
        start_card ENDL FLOAT FLOAT FLOAT ENDL node_constraints ENDL
        {
          double v3 = $3.fval;
          double v4 = $4.fval;
          double v5 = $5.fval;

          DEBUG("node: " << v3 << ", " << v4 << ", " << v5);

          observer_ptr->vertex(v3, v4, v5);
        }
        ;

node_constraints:
        NODE_CARD_CONSTRAINTS_ID INT INT INT INT
        {
          int v2 = $2.ival;
          int v3 = $3.ival;
          int v4 = $4.ival;
          int v5 = $5.ival;

          DEBUG("node_constraints: " << v2 << ", " << v3 << ", " 
                  << v4 << ", " << v5);
        }
        ;



elements: 
        element
        | elements element
        ;

element: 
        start_card ENDL element_global_data ENDL INT INT INT INT ENDL
        {
          int v1 = $1.ival;
          int v3 = $3.ival;
          int n0 = $5.ival;
          int n1 = $6.ival;
          int n2 = $7.ival;
          int n3 = $8.ival;

          DEBUG("element: " << v1 << ", " << v3 << ", "
                  << n0 << ", " << n1 << ", " << n2 << ", " << n3);

          observer_ptr->element(v1, v3, n0, n1, n2, n3);
        }
        |
        start_card ENDL element_global_data ENDL INT INT INT ENDL
        {
          int v1 = $1.ival;
          int v3 = $3.ival;
          int n0 = $5.ival;
          int n1 = $6.ival;
          int n2 = $7.ival;

          DEBUG("element: " << v1 << ", " << v3 << ", "
                  << n0 << ", " << n1 << ", " << n2);

          observer_ptr->element(v1, v3, n0, n1, n2, -1);
        }
        ;

element_global_data:
        INT INT INT INT FLOAT FLOAT FLOAT
        {
          int    v1 = $1.ival;
          int    v2 = $2.ival;
          int    v3 = $3.ival;
          int    v4 = $4.ival;
          double v5 = $5.fval;
          double v6 = $6.fval;
          double v7 = $7.fval;

          DEBUG("element_global_data: " << v1 << ", " << v2 << ", " 
                  << v3 << ", " << v4 << ", " << v5 << ", " 
                  << v6 << ", " << v7);

          $$ = $3;
        }
        ;



material_properties:
        start_card ENDL ninety_six_floats
        ;

ninety_six_floats:
        FLOAT 
        {
          double v1 = $1.fval;
          DEBUG("ninety_six_floats (FLOAT): " << v1);
        }
        | ninety_six_floats FLOAT
        {
          double v2 = $2.fval;
          DEBUG("ninety_six_floats (FLOAT): " << v2);
        }
        | ninety_six_floats FLOAT ENDL
        {
          double v2 = $2.fval;
          DEBUG("ninety_six_floats (FLOAT): " << v2);
        }
        ;



element_properties:
        start_card ENDL FLOAT ENDL
        {
          double v3 = $3.fval;
          DEBUG("element_properties: " << v3);
        }
        ;




element_loads:
        element_load
        | element_loads element_load
        ;

element_load:
        start_card ENDL INT INT ENDL FLOAT ENDL
        {
          int v3 = $3.ival;
          int v4 = $4.ival;
          int v6 = $6.ival;
          DEBUG("element_load: " << v3 << ", " << v4 << ", " << v6);
        }
        ;

%%

