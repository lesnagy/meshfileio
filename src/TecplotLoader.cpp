#include "TecplotLoader.h"

#include "DebugMacros.h"

#include "tecplot_bison.h"
#include "tecplot_flex.h"

#define YYTEC_DEBUG 1

void yytec_error(yyscan_t scanner, char const *msg)
{
  //std::cout << msg << std::endl;
}

void yytec_error(yyscan_t scanner, TecplotObserverPtr msgp, char const *msg)
{
  //std::cout << msg << std::endl;
}

TecplotLoader::TecplotLoader(TecplotObserver & observer) :
  mObserver(observer)
{}

void TecplotLoader::load(const std::string & fileName)
{
  // yytec_debug = 1;
  FILE *myfile = fopen(fileName.c_str(), "r");

  if (!myfile) {
    ERROR("Can't open input file: '" << fileName << "'");
    return;
  }

  yyscan_t myscanner;
  
  if (yytec_lex_init(&myscanner)) {
    ERROR("Error creating parser");
    return;
  }

  do {
    yytec_set_in(myfile, myscanner);
    if (yytec_parse(myscanner, &mObserver)) {
      ERROR("Parsing error");
    } 
  } while (!feof(myfile));
  yytec_lex_destroy(myscanner);
}
