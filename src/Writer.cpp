#include "Writer.h"

Writer::Writer():
  mFileName("")
{
}

Writer::Writer(const std::string & fileName):
  mFileName(fileName)
{
}

Writer::~Writer()
{
  if (mFout.is_open()) {
    mFout.close();
  }
}

std::string Writer::fileName() const 
{
  return mFileName;
}

void Writer::fileName(const std::string & fileName) 
{
  mFileName = fileName;
}

void Writer::begin()
{
  if (!mFout.is_open()) {
    mFout.open(mFileName);
  }
}

void Writer::end()
{
  if (mFout.is_open()) {
    mFout.close();
  }
}

