%option reentrant
%option bison-bridge
%option noyywrap nounput noinput
%option prefix="yytec_"

%{
#include "tecplot_bison.h"
%}

%%

[ \t]                                 ; // Action is ';' ignore all whitespace

TITLE                                 { return TITLE; }

VARIABLES                             { return VARIABLES; }

ZONE                                  { return ZONE; }

FEPOINT                               { return FEPOINT; }

TETRAHEDRON                           { return TETRAHEDRON; }

VARSHARELIST                          { return VARSHARELIST; }

CONNECTIVITYSHAREZONE                 { return CONNECTIVITYSHAREZONE; }
                
=                                     { return EQUAL_SYMBOL; }

,                                     { return COMMA_SYMBOL; }

T                                     { return T_SYMBOL; }

N                                     { return N_SYMBOL; }

E                                     { return E_SYMBOL; }

F                                     { return F_SYMBOL; }

ET                                    { return ET_SYMBOL; }

\(                                    { return LPAREN_SYMBOL; }

\)                                    { return RPAREN_SYMBOL; }

\[[0-9]+-[0-9]+\]                     { return RANGE; }

\"(\\.|[^\\"])*\"                     { 
                                        yylval->sval = strdup(yytext);
                                        return STRING_LITERAL;
                                      }

[0-9]+                                { 
                                        yylval->ival = atoi(yytext);
                                        return INT;
                                      }

-?([0-9]*\.[0-9]+([eE][-+]?[0-9]+)?)  { 
                                        yylval->fval = atof(yytext);
                                         return FLOAT; 
                                      }


\n                                    { 
                                        return ENDL; 
                                      }

.                                     {};

%%

