%option reentrant
%option bison-bridge
%option noyywrap nounput noinput
%option prefix="yypat_"

%{
#include "patran_bison.h"
%}

%%

[ \t]                                 ; // Action is ';' ignore all whitespace

0G                                    { return NODE_CARD_CONSTRAINTS_ID; }

-?([0-9]*\.[0-9]+([eE][-+]?[0-9]+)?)  { 
                                        yylval->fval = atof(yytext);
                                        return FLOAT; 
                                      }

[0-9]+                                { 
                                        yylval->ival = atoi(yytext);
                                        return INT;
                                      }

[a-zA-Z0-9]+                          {  
                                        yylval->sval = strdup(yytext);
                                        return STRING;
                                      }

[0-9]+:[0-9]+:[0-9]+                  {
                                        yylval->sval = strdup(yytext);
                                        return TIME;
                                      }

[0-9]+\.[0-9]+\.[0-9]+                {
                                        yylval->sval = strdup(yytext);
                                        return VERSION;
                                      }

[0-9]+\/[0-9]+\/[0-9]+                {
                                        yylval->sval = strdup(yytext);
                                        return DATE;
                                      }

\n                                    { return ENDL; }

.                                     {};

%%

