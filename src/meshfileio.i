%module(package="cpp", directors=1) meshfileio

%{
#include "ElementIndexing.h"
#include "ElementType.h"
#include "Writer.h"
#include "PatranLoader.h"
#include "PatranObserver.h"
#include "PatranWriter.h"
#include "TecplotLoader.h"
#include "TecplotObserver.h"
#include "TecplotWriter.h"
#include "DolfinLoader.h"
#include "DolfinObserver.h"

#include "refimpl/Field2D.hpp"
#include "refimpl/Field3D.hpp"
#include "refimpl/Tuple3.hpp"
#include "refimpl/Tuple4.hpp"
#include "refimpl/Vertex2D.hpp"
#include "refimpl/Vertex3D.hpp"

#include "refimpl/PatranIOImpl.h"
#include "refimpl/TecplotIOImpl.h"
#include "refimpl/DolfinIOImpl.h"
%}

%include <std_string.i>
%include <std_vector.i>

namespace std
{
  %template(VectorField2D) vector<Field2D>;
  %template(VectorField3D) vector<Field3D>;
  %template(VertexList2D)  vector<Vertex2D>;
  %template(VertexList3D)  vector<Vertex3D>;
  %template(TupleList3)    vector<Tuple3>;
  %template(TupleList4)    vector<Tuple4>;
  %template(VariableList)  vector<string>;
}

%apply int *OUTPUT { int * nzones };

%include "ElementIndexing.h"
%include "ElementType.h"
%include "Writer.h"
%include "PatranLoader.h"
%include "PatranObserver.h"
%include "PatranWriter.h"
%include "TecplotLoader.h"
%include "TecplotObserver.h"
%include "TecplotWriter.h"
%include "DolfinLoader.h"
%include "DolfinObserver.h"

%include "refimpl/Field2D.hpp"
%include "refimpl/Field3D.hpp"
%include "refimpl/Tuple3.hpp"
%include "refimpl/Tuple4.hpp"
%include "refimpl/Vertex2D.hpp"
%include "refimpl/Vertex3D.hpp"

%include "refimpl/PatranIOImpl.h"
%include "refimpl/TecplotIOImpl.h"
%include "refimpl/DolfinIOImpl.h"
