from sympy.vector import CoordSysCartesian
from sympy.tensor.array import Array, tensorproduct
from sympy import *

N = CoordSysCartesian('N')

x1 = symbols('x')
y1 = symbols('y')
z1 = symbols('z')

x2 = symbols('rhs.getx()')
y2 = symbols('rhs.gety()')
z2 = symbols('rhs.getz()')

v1 = x1*N.i + y1*N.j + z1*N.k
v2 = x2*N.i + y2*N.j + z2*N.k

v1_cross_v2 = v1.cross(v2)

print(v1_cross_v2.dot(N.i))
print(v1_cross_v2.dot(N.j))
print(v1_cross_v2.dot(N.k))













theta = symbols('theta')
ux    = symbols('u.x')
uy    = symbols('u.y')
uz    = symbols('u.z')
U     = Array([ux, uy, uz])

Ux    = Array([
    [  0, -uz,  uy],
    [ uz,   0, -ux],
    [-uy,  ux,   0]])

UU = tensorproduct(U,U) 

I  = eye(3)

R = cos(theta)*I + sin(theta)*Ux.tomatrix() + (1-cos(theta))*UU.tomatrix()


a00 = symbols('R[0][0]')
a01 = symbols('R[0][1]')
a02 = symbols('R[0][2]')
a10 = symbols('R[1][0]')
a11 = symbols('R[1][1]')
a12 = symbols('R[1][2]')
a20 = symbols('R[2][0]')
a21 = symbols('R[2][1]')
a22 = symbols('R[2][2]')

M = Matrix([
    [a00, a01, a02],
    [a10, a11, a12],
    [a20, a21, a22]])

detM = M.det()
invM = M.inv()
print(simplify(invM*detM))
print(detM)



