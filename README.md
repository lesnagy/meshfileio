## Introduction

This is a small project containing utility functions to load/save files
containing mesh information. The most important information is 
  1. coordinates of vertices
  2. lists of indices defining elements
  3. vector field values at points

## Building

CMake is used to create build files. It is recommended to use out of source
builds (i.e. create a separate directory) and then do the following

    $> cmake <path to meshfileio>
    $> make prereq
    $> make

**NOTE** the `make prereq` command is important since this will pull down any 
prerequisited that the project depends on.

This will then build the libraries. In order to set a prefix install directory
run CMake with
    
    $> cmake -DCMAKE_INSTALL_PREFIX=<path to install> <path to meshfileio>
    $> make prereq
    $> make
    $> make install

## Other useful options

Other useful options for cmake include

    -DCMAKE_BUILD_TYPE=<Debug|Release>

Enable 'Debug' level messages

    -DCMAKE_CXX_FLAGS="-DDEBUG_MESSAGES -DDEBUG_MESSAGES_LEVEL=1"

Enable 'Info' level messages

    -DCMAKE_CXX_FLAGS="-DDEBUG_MESSAGES -DDEBUG_MESSAGES_LEVEL=2"

Enable 'Warn' level messages

    -DCMAKE_CXX_FLAGS="-DDEBUG_MESSAGES -DDEBUG_MESSAGES_LEVEL=3"

Enable 'Error' level messages

    -DCMAKE_CXX_FLAGS="-DDEBUG_MESSAGES -DDEBUG_MESSAGES_LEVEL=4"

Enable 'Severe error' level messages

    -DCMAKE_CXX_FLAGS="-DDEBUG_MESSAGES -DDEBUG_MESSAGES_LEVEL=5"

By default 'Warn' level messages are used and so if just `-DDEBUG_MESSAGES` is
used without `-DDEBUG_MESSAGES_LEVEL` it will revert to level 3 (warnings). 

BY default tests are built. You can disable building of tests using

    -DBUILD_TESTING=OFF

## Flex/Bison

The text file parsers are written using flex (version 2.5 or above) and bison
(version 3.0 or above). It is possible that CMake can't find the correct
version; for example on OSX it will use the system flex/bison which are pretty
old. If this is the case local versions of flex/bison may be build and assigned
to CMake with

    -DFLEX_EXECUTABLE=<path to flex>

and

    -DBISON_EXECUTABLE=<path to bison>


## Testing

If you also wish to build tests then run CMake with BUILD_TESTING enabled

    $> cmake -DBUILD_TESTING=ON <path to meshfileio>

test meshes will be downloaded and the make target `build-tests` will become 
available. Tests may then be built with

    $> make build-tests

and tests may be run with 

    $> make test

