#
# Library configuration file used by dependent projects.
# @file:   meshfileio-config.cmake
# @author: L. Nagy
#

if (NOT DEFINED meshfileio_FOUND)

  # Locate the library headers.
  find_path(meshfileio_include_dir
    NAMES PatranLoader.h
          TecplotLoader.h
          global_constants.h
          patran.h
          tecplot.h
    PATHS ${meshfileio_DIR}/include
  )

  # Locate libraries.
  find_library(meshfileio_libraries
    meshfileio
    PATHS ${meshfileio_DIR}/lib
  )

#  # Export libary targets.
#  set(meshfileio_libraries
#    meshfileio
#    CACHE INTERNAL "meshfileio library" FORCE
#  )

  # Handle REQUIRED, QUIED and version related arguments to find_package(...)
  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(
    meshfileio DEFAULT_MSG
    meshfileio_include_dir
    meshfileio_libraries
  )

#  # If the project is not `meshfileio` then make directories. 
#  if (NOT ${PROJECT_NAME} STREQUAL meshfileio)
#    add_subdirectory(
#      ${meshfileio_DIR}
#      ${CMAKE_CURRENT_BINARY_DIR}/meshfileio
#    )
#  endif()
endif()
